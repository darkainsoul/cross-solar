﻿using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Domain
{
    public class Panel
    {
        public int Id { get; set; }

        [Required, Range(0, 999999999.999999)] 
        public double Latitude { get; set; }
        
        [Required, Range(0, 999999999.999999)] 
        public double Longitude { get; set; }

        [Required, StringLength(16)] 
        public string Serial { get; set; }

        public string Brand { get; set; }
    }
}