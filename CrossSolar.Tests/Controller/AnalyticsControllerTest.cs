﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrossSolar.Controllers;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using Xunit;

namespace CrossSolar.Tests.Controller
{
    public class AnalyticsControllerTest
    {
        private readonly AnalyticsController _analyticsController;
        private readonly PanelController _panelController;
        
        private readonly Mock<IAnalyticsRepository> _analyticsRepositoryMock = new Mock<IAnalyticsRepository>();
        private readonly Mock<IPanelRepository> _panelRepositoryMock = new Mock<IPanelRepository>();

        private const string PanelId = "XXXX1111YYYY2222";

        public AnalyticsControllerTest()
        {
            var analyticsData = new List<OneHourElectricity>()
            {
                new OneHourElectricity
                {
                    PanelId = PanelId,
                    DateTime = DateTime.UtcNow,
                    KiloWatt = 1
                },
                new OneHourElectricity
                {
                    PanelId = PanelId,
                    DateTime = DateTime.UtcNow,
                    KiloWatt = 2
                }
            }.AsQueryable().BuildMock();
            
            var panelsData = new List<Panel>()
            {
                new Panel
                {
                    Brand = "Areva",
                    Latitude = 12.345678,
                    Longitude = 98.7655432,
                    Serial = PanelId
                }
            }.AsQueryable().BuildMock();

            _analyticsRepositoryMock = new Mock<IAnalyticsRepository>();
            _analyticsRepositoryMock.Setup(x => x.Query()).Returns(analyticsData.Object);

            _panelRepositoryMock = new Mock<IPanelRepository>();
            _panelRepositoryMock.Setup(x => x.Query()).Returns(panelsData.Object);

            _analyticsController = new AnalyticsController(_analyticsRepositoryMock.Object, _panelRepositoryMock.Object);
        }
        
        
        [Fact]
        public async Task Get_DayResults()
        {
            var result = await _analyticsController.DayResults(PanelId);
            Assert.NotNull(result);
            var createdResult = result as CreatedResult;
            Assert.Null(createdResult);
        }

        [Fact]
        public async Task Get_Analytic()
        {
            var actionResult = await _analyticsController.Get(PanelId);
            var okObjectResult = actionResult as OkObjectResult;
            Assert.NotNull(okObjectResult);

            var model = okObjectResult.Value as OneHourElectricityListModel;
            Assert.NotNull(model);
            Assert.NotNull(model.OneHourElectricitys);
            Assert.Equal(2, model.OneHourElectricitys.Count());
        }

        [Fact]
        public async Task Post_OneHourElectricity()
        {
            var oneHourElectricityContent = new OneHourElectricityModel
            {
                KiloWatt = 11,
                DateTime = DateTime.UtcNow
            };

            var result = await _analyticsController.Post(PanelId, oneHourElectricityContent);

            Assert.NotNull(result);
            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.Equal(201, createdResult.StatusCode);
        }
    }
}