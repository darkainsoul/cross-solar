﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using CrossSolar.Domain;
using Xunit;

namespace CrossSolar.Tests.Models
{
    public class OneDayElectricityModelTest
    {
        [Fact]
        public void ValidatePanelModel_Range()
        {
            var oneDayElectricityModelValid = new OneDayElectricityModel
            {
                DateTime = DateTime.UtcNow,
                Sum = 10,
                Average = 100,
                Minimum = 1,
                Maximum = 40
            };

            var context1 = new ValidationContext(oneDayElectricityModelValid, null, null);
            var results1 = new List<ValidationResult>();
            var isModelStateValid = Validator.TryValidateObject(oneDayElectricityModelValid, context1, results1, true);
            Assert.True(isModelStateValid);
        }
    }
}
